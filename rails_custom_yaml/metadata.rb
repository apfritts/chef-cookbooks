name        "rails_custom_yaml"
description "Creates custom YAML files in the config/ directory of your rails application."
maintainer  "AP Endeavors"
license     "Apache 2.0"
version     "1.0.0"

depends "deploy"

recipe "rails_custom_yaml::configure", "Configure a Rails application with attributes from node[:custom_yaml]"

