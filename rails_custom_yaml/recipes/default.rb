include_recipe "deploy"

node[:deploy].each do |application, deploy|
  deploy = node[:deploy][application]

  execute "restart Rails app #{application}" do
    cwd deploy[:current_path]
    command node[:opsworks][:rails_stack][:restart_command]
    action :nothing
  end

  deploy = node[:deploy][application]
  Chef::Log.info("I #{node[:custom_yaml].present? ? 'found some' : 'did not find any'} custom YAML.")

  template "#{deploy[:deploy_to]}/shared/config/custom_yaml.yml" do
    source "default.yml.erb"
    mode "0660"
    group deploy[:group]
    owner deploy[:user]
    variables(:custom_yaml => node[:custom_yaml], :environment => deploy[:rails_env])

    notifies :run, "execute[restart Rails app #{application}]"

    only_if do
      ret = node[:custom_yaml].present? and ::File.exists?("#{deploy[:deploy_to]}/shared/config/")
      if node[:custom_yaml].present?
        Chef::Log.info('I **should** generate a fun file for you.')
        if ret
          Chef::Log.info('I will generate a fun file for you.')
        else
          Chef::Log.error('I will **not** generate a fun file for you.')
        end
      end
      ret
    end
  end
end
